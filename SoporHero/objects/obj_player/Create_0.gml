/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4F60B20A
/// @DnDArgument : "code" "globalvar vface;$(13_10)globalvar hface;$(13_10)globalvar key_up;$(13_10)globalvar key_down;$(13_10)globalvar key_left;$(13_10)globalvar key_right;$(13_10)globalvar last_key;$(13_10)$(13_10)globalvar spd_v;$(13_10)globalvar spd_h;$(13_10)globalvar max_spd;$(13_10)globalvar acc;$(13_10)globalvar defeat;$(13_10)globalvar temp;$(13_10)globalvar phone_ring;$(13_10)globalvar obj_actual;$(13_10)globalvar sprt_actual;$(13_10)globalvar rand_obj;$(13_10)$(13_10)// Variable pour les Malus (Optimisation des objets)//$(13_10)globalvar boolPhone;$(13_10)globalvar boolVoisin;$(13_10)globalvar boolExtasy;$(13_10)globalvar boolEnergyDrink;$(13_10)$(13_10)boolPhone = 0;$(13_10)boolVoisin = 0;$(13_10)boolExtasy = 0;$(13_10)boolEnergyDrink = 0;$(13_10)$(13_10)// Variable pour les Assets //$(13_10)globalvar boolCana;$(13_10)globalvar boolTea;$(13_10)globalvar boolPill;$(13_10)globalvar boolBook;$(13_10)globalvar boolLight;$(13_10)globalvar boolCloth;$(13_10)$(13_10)boolBook = [];$(13_10)boolCana = 0;$(13_10)boolTea = 0;$(13_10)boolPill = 0;$(13_10)boolBook = 0;$(13_10)boolCloth = 0;$(13_10)$(13_10)$(13_10)key_up = keyboard_check(vk_up);$(13_10)key_down  = keyboard_check(vk_down);$(13_10)key_left = keyboard_check(vk_left);$(13_10)key_right = keyboard_check(vk_right);$(13_10)rand_obj = 0;$(13_10)sprt_actual[0] = spr_collision;$(13_10)sprt_actual[1] = sprt_ganja;$(13_10)sprt_actual[2] = sprt_tea;$(13_10)sprt_actual[3] = sprt_book;$(13_10)sprt_actual[4]= sprt_pills;$(13_10)sprt_actual[5] = sprt_cloth;$(13_10)rand_obj[0] = 1;$(13_10)rand_obj[1] = 2;$(13_10)rand_obj[2] = 3;$(13_10)rand_obj[3] = 4;$(13_10)rand_obj[4] = 5;$(13_10)hface = 1;$(13_10)vface = 1;$(13_10)spd_h = 0;$(13_10)spd_v = 0;$(13_10)max_spd = 5;$(13_10)acc = 0.3;$(13_10)last_key = 0;$(13_10)defeat = 0;$(13_10)phone_ring = 0;$(13_10)temp = 0;$(13_10)$(13_10)audio_play_sound(sound_3, 0, 1);$(13_10)dsound = 1;"

{
	globalvar vface;
globalvar hface;
globalvar key_up;
globalvar key_down;
globalvar key_left;
globalvar key_right;
globalvar last_key;

globalvar spd_v;
globalvar spd_h;
globalvar max_spd;
globalvar acc;
globalvar defeat;
globalvar temp;
globalvar phone_ring;
globalvar obj_actual;
globalvar sprt_actual;
globalvar rand_obj;

// Variable pour les Malus (Optimisation des objets)//
globalvar boolPhone;
globalvar boolVoisin;
globalvar boolExtasy;
globalvar boolEnergyDrink;

boolPhone = 0;
boolVoisin = 0;
boolExtasy = 0;
boolEnergyDrink = 0;

// Variable pour les Assets //
globalvar boolCana;
globalvar boolTea;
globalvar boolPill;
globalvar boolBook;
globalvar boolLight;
globalvar boolCloth;

boolBook = [];
boolCana = 0;
boolTea = 0;
boolPill = 0;
boolBook = 0;
boolCloth = 0;


key_up = keyboard_check(vk_up);
key_down  = keyboard_check(vk_down);
key_left = keyboard_check(vk_left);
key_right = keyboard_check(vk_right);
rand_obj = 0;
sprt_actual[0] = spr_collision;
sprt_actual[1] = sprt_ganja;
sprt_actual[2] = sprt_tea;
sprt_actual[3] = sprt_book;
sprt_actual[4]= sprt_pills;
sprt_actual[5] = sprt_cloth;
rand_obj[0] = 1;
rand_obj[1] = 2;
rand_obj[2] = 3;
rand_obj[3] = 4;
rand_obj[4] = 5;
hface = 1;
vface = 1;
spd_h = 0;
spd_v = 0;
max_spd = 5;
acc = 0.3;
last_key = 0;
defeat = 0;
phone_ring = 0;
temp = 0;

audio_play_sound(sound_3, 0, 1);
dsound = 1;
}

