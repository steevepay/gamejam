{
    "id": "4e5547b5-9c54-4e7b-9e26-48ab57a69816",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_room",
    "eventList": [
        {
            "id": "79ce0672-5213-4e24-b325-66fd2e24d507",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4e5547b5-9c54-4e7b-9e26-48ab57a69816"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "3c3b5683-e95d-4258-9e50-8dda781d2094",
    "visible": true
}